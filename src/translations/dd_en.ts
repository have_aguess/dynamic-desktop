<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../forms/aboutdialog.ui" line="14"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/aboutdialog.ui" line="24"/>
        <source>Get source code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/aboutdialog.ui" line="31"/>
        <source>Report suggestions or issues</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/aboutdialog.ui" line="38"/>
        <source>Get the latest release</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/aboutdialog.ui" line="53"/>
        <source>Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/aboutdialog.ui" line="102"/>
        <source>Commit ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/aboutdialog.ui" line="145"/>
        <source>Commit time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/aboutdialog.ui" line="231"/>
        <source>Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/aboutdialog.ui" line="274"/>
        <source>QtAV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/aboutdialog.ui" line="317"/>
        <source>FFmpeg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/aboutdialog.ui" line="360"/>
        <source>Compiler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/aboutdialog.ui" line="403"/>
        <source>Architecture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/aboutdialog.ui" line="440"/>
        <source>About Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/aboutdialog.ui" line="188"/>
        <source>Build time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/aboutdialog.ui" line="460"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../forms/preferencesdialog.ui" line="23"/>
        <location filename="../forms/preferencesdialog.ui" line="137"/>
        <source>Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.ui" line="247"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.ui" line="253"/>
        <source>Auto start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.ui" line="309"/>
        <source>Player</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.ui" line="386"/>
        <source>Video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.ui" line="394"/>
        <source>Try to use Hardware Accelerated Decoding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.ui" line="414"/>
        <source>CUDA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.ui" line="421"/>
        <source>D3D11</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.ui" line="465"/>
        <source>Keep video aspect ratio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.ui" line="472"/>
        <source>Fit desktop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.cpp" line="33"/>
        <source>Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.cpp" line="32"/>
        <source>Best</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.cpp" line="31"/>
        <source>Fastest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.ui" line="509"/>
        <source>Rendering engine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.ui" line="529"/>
        <source>Video track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.ui" line="603"/>
        <source>Auto load external audio tracks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.ui" line="627"/>
        <source>Audio track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.ui" line="428"/>
        <source>DXVA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.ui" line="268"/>
        <source>Skin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.ui" line="288"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.ui" line="323"/>
        <location filename="../forms/preferencesdialog.cpp" line="278"/>
        <source>URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.ui" line="336"/>
        <source>Local files and web urls are all acceptable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.ui" line="343"/>
        <source>Input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.ui" line="350"/>
        <source>Browse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.ui" line="445"/>
        <source>Picture ratio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.ui" line="489"/>
        <source>Output image quality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.ui" line="549"/>
        <source>Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.ui" line="577"/>
        <source>Audio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.ui" line="585"/>
        <source>Volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.ui" line="361"/>
        <source>Play</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.ui" line="368"/>
        <source>Pause</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.ui" line="648"/>
        <source>Subtitle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.ui" line="654"/>
        <source>Display subtitle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.ui" line="663"/>
        <source>Auto load external subtitle tracks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.ui" line="610"/>
        <location filename="../forms/preferencesdialog.ui" line="670"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.ui" line="687"/>
        <source>Subtitle track</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.ui" line="707"/>
        <source>Charset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.ui" line="724"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.ui" line="744"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.ui" line="751"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.cpp" line="79"/>
        <source>Please select an audio file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.cpp" line="79"/>
        <source>Audios (*.mka *.aac *.flac *.mp3 *.wav);;All files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.cpp" line="86"/>
        <source>Please select a subtitle file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.cpp" line="109"/>
        <location filename="../forms/preferencesdialog.cpp" line="128"/>
        <location filename="../forms/preferencesdialog.cpp" line="149"/>
        <source>ID: %0 | Title: %1 | Language: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.cpp" line="156"/>
        <source>File: %0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.cpp" line="74"/>
        <source>auto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.cpp" line="86"/>
        <source>Subtitles (*.ass *.ssa *.srt *.sub);;All files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.cpp" line="270"/>
        <source>Please select a media file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.cpp" line="270"/>
        <source>Videos (*.avi *.mp4 *.mkv *.flv);;Audios (*.mp3 *.flac *.ape *.wav);;Pictures (*.bmp *.jpg *.jpeg *.png *.gif);;All files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.cpp" line="278"/>
        <source>Please input a valid URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.cpp" line="290"/>
        <source>&quot;%0&quot; is not a valid URL.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.cpp" line="301"/>
        <location filename="../forms/preferencesdialog.cpp" line="307"/>
        <location filename="../forms/preferencesdialog.cpp" line="313"/>
        <location filename="../forms/preferencesdialog.cpp" line="319"/>
        <source>Reopen this video or play another video to experience it.
Make sure this application runs in your GPU&apos;s Optimus mode.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.cpp" line="321"/>
        <source>Auto detect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/preferencesdialog.cpp" line="322"/>
        <source>System</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../main.cpp" line="202"/>
        <source>This application only supports Windows 7 and newer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="208"/>
        <source>There is another instance running. Please do not run twice.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="213"/>
        <source>A tool that make your desktop alive.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="295"/>
        <source>WARNING: You are running a debug version of this tool!
Do not continue running it if you are not a developer!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="306"/>
        <location filename="../main.cpp" line="674"/>
        <source>Current renderer is not available on your platform!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="420"/>
        <source>Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="438"/>
        <source>Play</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="444"/>
        <source>Pause</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="451"/>
        <source>Mute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="465"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="482"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../main.cpp" line="236"/>
        <source>renderer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="224"/>
        <source>Play the given url. It can be a local file or a valid web url. Default is empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="217"/>
        <source>Show a normal window instead of placing it under the desktop icons.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="220"/>
        <source>Set skin. The value is the file name of the skin file, excluding the file extension. If it&apos;s not under the &quot;skins&quot; folder, please give the absolute path of the file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="221"/>
        <source>Skin file name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="231"/>
        <source>Set the quality of the output image. It can be default/best/fastest. Default is fastest. Case insensitive.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="232"/>
        <source>Image quality</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="235"/>
        <source>Set rendering engine. It can be opengl/gl/qt/gdi/d2d. Default is gl. Case insensitive.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="239"/>
        <source>Set volume. It must be a positive integer between 0 and 99. Default is 9.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="225"/>
        <source>url</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="240"/>
        <source>volume</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
